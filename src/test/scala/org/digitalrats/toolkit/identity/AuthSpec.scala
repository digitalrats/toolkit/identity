package org.digitalrats.toolkit.identity

import java.time.Instant
import java.util.UUID

import akka.actor.testkit.typed.scaladsl.ActorTestKit
import akka.actor.typed.ActorSystem
import akka.http.scaladsl.model.{StatusCode, StatusCodes}
import cats.implicits._
import org.digitalrats.toolkit.identity.Token.{JWTIssuer, JWTKey}
import org.digitalrats.toolkit.identity.Vault.VaultToken
import org.scalatest.BeforeAndAfterAll
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

import scala.concurrent.{Await, ExecutionContextExecutor, Future}
import scala.concurrent.duration._

class AuthSpec extends AnyWordSpec
  with BeforeAndAfterAll
  with Matchers {

  val testKit: ActorTestKit = ActorTestKit()
  implicit val system: ActorSystem[Nothing] = testKit.system
  override def afterAll(): Unit = testKit.shutdownTestKit()

  private val mokUser = "moyshe"
  private val mokPassword = "aeqrfdgh"
  private val mokFirstName = "Moyshe"
  private val mokLastName = "benRabbi"
  private val mokRole = "user"


  private implicit val vaultToken: VaultToken = VaultToken("s.sxYTMuEDYG57qdkQ0V79O6iN")
  private implicit val issuer: JWTIssuer = JWTIssuer("https://scaffold.me")
  private implicit val jwtkey: JWTKey = JWTKey(Vault.read("auth"))

  implicit val ec: ExecutionContextExecutor = system.executionContext

  /// \todo Возможно, нужен актор, который инициализирует Ignite и создаст сервер. Теоретически в этом акторе ждать SIGINT

  "A Signup" when {
    val registrar = testKit.spawn(Registrar(),"reg")
    val probe = testKit.createTestProbe[StatusCode]()
    "registrar" should {"create new user and return StatusCode.OK" in {
      registrar ! Registrar.SignRequest(SignupRequest(mokUser,mokPassword,mokFirstName,mokLastName), probe.ref)
      probe.expectMessage(StatusCodes.OK)
    }}
  }

  "A Login" when {
    val authenticator = testKit.spawn(Authorizer(),"auth")
    val probe = testKit.createTestProbe[Either[StatusCode,Profile]]()
    "unknown" should { "return Unautorized" in {
      authenticator ! Authorizer.AuthRequest(LoginRequest("Test", "Test"), probe.ref)
      probe.expectMessage(StatusCodes.Unauthorized.asLeft)
    }}
    "known with wrong password" should { "return Unautorized" in {
      authenticator ! Authorizer.AuthRequest(LoginRequest(mokUser, "Test"), probe.ref)
      probe.expectMessage(StatusCodes.Unauthorized.asLeft)
    }}
    "known" should { "return a Profile" in {
      authenticator ! Authorizer.AuthRequest(LoginRequest(mokUser, mokPassword), probe.ref)
      probe.expectMessage(Profile(mokFirstName,mokLastName,mokRole).asRight)
    }}
    "decoder" should { "decode token" in {
      val token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL3NjYWZmb2xkLm1lIiwic3ViIjoiYXV0aHwwMDAwMDAwMC0wMDAwLTAwMDAtMDAwMC0wMDAwMDAwMDAwMDAiLCJleHAiOjE3NDUzNDAyNTksImlhdCI6MTU4NzU1NTQ5OX0.m0s4MQz7mbTgUGWtcleE00orNdRK1BzG6NjHZ7ED3ps"
      val uuid = Await.result(Token.decode(token), 10 minute).getOrElse("Error") // It is best to set it's timeout not to Duration.Inf
      assert(uuid == new UUID(0,0))
    }}
    "tokenizer" should { "return a valid token" in {
      val uuid = new UUID(0,0)
      val token = Await.result(Token.create(new Session(uuid,Instant.now.plusSeconds( 157784760).getEpochSecond)), 10 minute).getOrElse("Error") // It is best to set it's timeout not to Duration.Inf
      assert(token.length == 232) // Token is time
    }}
    "session" should { "create a Session object" in {
      implicit val expirationOffset: Long = 157784760
      val session:Session = Await.result(Session.create(mokUser),10 second).getOrElse(null)
      assert(session != null)
    }}
  }
}
