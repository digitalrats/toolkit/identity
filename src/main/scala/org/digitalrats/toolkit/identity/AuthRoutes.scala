package org.digitalrats.toolkit.identity

import akka.actor.typed.scaladsl.{ActorContext, Behaviors, Routers}
import akka.actor.typed.{ActorRef, ActorSystem, SupervisorStrategy}

class AuthRoutes(ctx: ActorContext[Server.Message])(implicit system: ActorSystem[_]) {
  import akka.actor.typed.scaladsl.AskPattern._
  import akka.http.scaladsl.model.{ContentTypes, HttpEntity, StatusCode, StatusCodes}
  import akka.http.scaladsl.server.Directives.{as, complete, concat, entity, get, path, post}
  import akka.http.scaladsl.server.Route
  import akka.util.Timeout
  import cats.data.EitherT
  import cats.implicits._
  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
  import io.circe.Json
  import io.circe.generic.auto._
  import io.circe.syntax._

  import scala.concurrent.ExecutionContext.Implicits.global
  import scala.concurrent.Future
  import scala.concurrent.duration._
  import org.digitalrats.toolkit.identity.Token.JWTIssuer
  import org.digitalrats.toolkit.identity.Token.JWTKey
  import org.digitalrats.toolkit.identity.Vault.VaultToken

  private implicit val timeout: Timeout = 3 seconds
  private implicit val expirationOffset: Long = 157784760

  private implicit val vaultToken: VaultToken = VaultToken("s.sxYTMuEDYG57qdkQ0V79O6iN")
  private implicit val issuer: JWTIssuer = JWTIssuer("https://scaffold.me")
  private implicit val key: JWTKey = JWTKey(Vault.read("auth"))


  private lazy val authenticatorPool = Routers.pool(poolSize = 8)(
    // make sure the workers are restarted if they fail
    Behaviors.supervise(Authorizer()).onFailure[Exception](SupervisorStrategy.restart))

  private lazy val registrarPool = Routers.pool(poolSize = 8)(
    // make sure the workers are restarted if they fail
    Behaviors.supervise(Registrar()).onFailure[Exception](SupervisorStrategy.restart))

  private lazy val registrar = ctx.spawn(registrarPool, "Registrar")

  private lazy val authenticator = ctx.spawn(authenticatorPool, "worker-pool")

  lazy val theAuthRoutes: Route = concat(
    get {
      concat(
        path("") {
          complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "<h1>Say hello to toolkit identity</h1>"))
        },
        path("health") {
          /** \todo Релизовать это */
          complete(StatusCodes.OK)
        }
      )
    },
    post {
      concat(
        path("signup") {
          entity(as[SignupRequest]) { request =>
            complete(
              registrar.ask[StatusCode](Registrar.SignRequest(request, _))
            )
          }
        },
        path("login") {
          entity(as[LoginRequest]) { request =>
            complete(
              (for {
                profile <- EitherT[Future, StatusCode, Profile](authenticator.ask(Authorizer.AuthRequest(request, _)))
                session <- EitherT(Session.create(request.login))
                token <- EitherT(Token.create(session))
                response <- EitherT(Future.successful({
                  (new LoginResponse(profile.role, token)).asJson.asRight[StatusCode]
                }))
              } yield response).value
            )
          }
        }
      )
    }
  )
}
