package org.digitalrats.toolkit.identity

import akka.actor.ActorSystem

import scala.concurrent.Future;

class IdentityHealthCheck(system: ActorSystem) extends (() => Future[Boolean]) {
  override def apply(): Future[Boolean] = {
    /** \todo Релизовать это */
    Future.successful(true)
  }
}