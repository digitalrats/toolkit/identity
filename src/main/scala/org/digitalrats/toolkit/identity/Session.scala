package org.digitalrats.toolkit.identity

import java.util.UUID

import akka.http.scaladsl.model.StatusCodes

final case class Session(id: UUID, expiration: Long)

object Session {

  import akka.http.scaladsl.model.StatusCode
  import cats.implicits._
  import io.circe.generic.auto._
  import io.circe.syntax._

  import scala.annotation.tailrec
  import scala.concurrent.Future
  import java.time.Instant

  private case class SessionBody(login: String, expiration: Long)
  /// \todo Брать это из конфигурации
  def create(login: String)(implicit expirationOffset:Long) : Future[Either[StatusCode, Session]] = {
    val cache = Model.session
    /// \todo Скорее всего, этого мало - нужен сервис-синглетон с жесткой блокировкой на уровне кластера
    @tailrec
    def newUUID:UUID = {
      val id = UUID.randomUUID()
      if (!cache.containsKey(id)) id else newUUID
    }
    val expiration: Long = Instant.now.plusSeconds(expirationOffset).getEpochSecond
    /// \todo Нужен сервис для удаления просроченных сессий.
    val str = SessionBody(login, expiration).asJson.noSpaces
    val id = newUUID
    cache.put(id, str)
    Future.successful(Session(id, expiration).asRight[StatusCode])
  }
}

