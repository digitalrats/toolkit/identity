package org.digitalrats.toolkit.identity


object Token {

  case class JWTIssuer(value: String)
  case class JWTKey(value: String)

  import java.time.Instant
  import java.util.UUID

  import akka.http.scaladsl.model.{StatusCode, StatusCodes}
  import cats.implicits._
  import pdi.jwt.{JwtAlgorithm, JwtCirce, JwtClaim}

  import scala.concurrent.ExecutionContext.Implicits.global
  import scala.concurrent.Future
  import scala.util.{Failure, Success}
  import org.digitalrats.toolkit.identity.Vault.VaultToken
  import pdi.jwt.algorithms.JwtHmacAlgorithm

  final def create(session: Session)(implicit issuer: JWTIssuer, key: JWTKey, algorithm:JwtHmacAlgorithm = JwtAlgorithm.HS256): Future[Either[StatusCode, String]] = Future.successful({
    val claim = JwtClaim(
      issuer = Some(issuer.value),
      subject = Some("auth|" + session.id),
      expiration = Some(session.expiration),
      issuedAt = Some(Instant.now.getEpochSecond)
    )
    JwtCirce.encode(claim, key.value, algorithm).asRight
  })


  final def decode(token: String)(implicit issuer: JWTIssuer, key: JWTKey, algorithm:JwtHmacAlgorithm = JwtAlgorithm.HS256): Future[Either[StatusCode, UUID]] = {
    val algorithm = JwtAlgorithm.HS256
    val result: Either[StatusCode,UUID] = JwtCirce.decode(token, key.value, List(algorithm)) match {
      case Success(claim) =>
        if (!claim.issuer.contains("https://scaffold.me")) {
          StatusCodes.BadRequest.asLeft
        } else if (claim.expiration >= Some(Instant.now.getEpochSecond)) {
          try {
            val uuid = claim.subject.getOrElse("").substring(5)
            UUID.fromString(uuid).asRight
          } catch {
            case e: Throwable => StatusCodes.BadRequest.asLeft
          }
        } else {
          StatusCodes.Forbidden.asLeft
        }
      case Failure(exception) =>
        StatusCodes.BadRequest.asLeft
    }
    Future.successful(result)
  }

}
