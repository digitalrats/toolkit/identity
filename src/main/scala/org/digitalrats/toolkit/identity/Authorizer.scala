package org.digitalrats.toolkit.identity

import akka.io.IO

import scala.concurrent.{ExecutionContextExecutor, Future}

object Authorizer {
  import akka.actor.typed.scaladsl.Behaviors
  import akka.actor.typed.{ActorRef, Behavior, PostStop}
  import akka.http.scaladsl.model.{StatusCode, StatusCodes}
  import cats.implicits._
  import com.github.t3hnar.bcrypt._
  import org.slf4j.Logger
  sealed trait Request

  final case class AuthRequest(request: LoginRequest, replyTo: ActorRef[Either[StatusCode,Profile]]) extends Request
  final case object GracefulShutdown extends Request


  def cleanup(log: Logger): Unit = {
    log.info("Cleaning up!")
  }

  def apply(): Behavior[Request] = Behaviors.receive[Request] { (context, message) => {

    def authorize(login: String, password: String): Either[StatusCode,Profile] =  {
      import Model.ignite
      val stored = Model.auth.get(login)
      if (stored == null) {
        StatusCodes.Unauthorized.asLeft
      } else {
        try {
          password.bcrypt(stored.salt).replace(stored.salt,"") match {
            case stored.hash => stored.profile.asRight
            case _ => StatusCodes.Unauthorized.asLeft
          }
        } catch {
          case e: Throwable => {
            StatusCodes.BadRequest.asLeft
          }
        }
      }
    }

    message match {
      case AuthRequest(request,  replyTo) =>
        replyTo ! authorize(request.login, request.password)
        Behaviors.same
      case GracefulShutdown =>
        context.log.info("Initiating graceful shutdown...")
        Behaviors.stopped { () =>
          cleanup(context.log)
        }
    }
  }
  }.receiveSignal {
    case (context, PostStop) =>
      context.log.info("Master Control Program stopped")
      cleanup(context.log)
      Behaviors.same
  }

}
