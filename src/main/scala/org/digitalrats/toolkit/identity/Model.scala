package org.digitalrats.toolkit.identity

import java.util.UUID

import org.apache.ignite.IgniteCache
import org.apache.ignite.spi.communication.tcp.TcpCommunicationSpi
import org.slf4j.Logger

case class Model(salt: String, hash: String, profile: Profile)

import org.apache.ignite.Ignite

/*
import org.apache.ignite.lang.IgniteRunnable
import org.apache.ignite.resources.IgniteInstanceResource

private class RemoteTask extends IgniteRunnable {
  /** Auto-injected instance of Ignite. */
  @IgniteInstanceResource val ignite: Ignite = null
  override def run(): Unit = {
    System.out.println(">> Executing the compute task")
    System.out.println("   Node ID: " + ignite.cluster.localNode.id + "\n" + "   OS: " + System.getProperty("os.name") + "   JRE: " + System.getProperty("java.runtime.name"))
    val cache:IgniteCache[Integer, String] = ignite.cache("myCache")
    System.out.println(">> " + cache.get(1) + " " + cache.get(2))
  }
}
*/

object Model {
  import java.util.Collections

  import org.apache.ignite.Ignition
  import org.apache.ignite.configuration.IgniteConfiguration
  import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi
  import org.apache.ignite.spi.discovery.tcp.ipfinder.multicast.TcpDiscoveryMulticastIpFinder

  val cfg = new IgniteConfiguration

  val tcpCommunicationSPI = new TcpCommunicationSpi
  tcpCommunicationSPI.setMessageQueueLimit(1024)

  cfg.setClientMode(true)
  cfg.setCommunicationSpi(tcpCommunicationSPI)
  cfg.setPeerClassLoadingEnabled(true)

  val ipFinder = new TcpDiscoveryMulticastIpFinder
  ipFinder.setAddresses(Collections.singletonList("127.0.0.1:47500..47509"))
  cfg.setDiscoverySpi(new TcpDiscoverySpi().setIpFinder(ipFinder))

  private val ignite: Ignite = Ignition.getOrStart(cfg)

  val auth:IgniteCache[String,Model] = ignite.getOrCreateCache("auth")
  val session:IgniteCache[UUID,String] = ignite.getOrCreateCache("session")

  /*
  import org.apache.ignite.IgniteCompute
  val compute: IgniteCompute = ignite.compute(cluster.forRemotes)
  ignite.compute(ignite.cluster.forServers).broadcast(new RemoteTask)
  */
  //val cache: Resource[IO, IgniteCache[String, Model]] = cache[String,Model]("auth")

  /*
  import org.apache.ignite.IgniteCluster
  val cluster: IgniteCluster = ignite.cluster

  import org.apache.ignite.services.ServiceConfiguration
  import org.digitalrats.toolkit.auth.AuthServiceImpl
  val svccfg = new ServiceConfiguration
  val authService = new AuthServiceImpl

  svccfg.setService(authService)
  svccfg.setName("AuthService")
  svccfg.setTotalCount(1)

  import org.apache.ignite.IgniteServices
  val svcs: IgniteServices = ignite.services(cluster.forServers())
  svcs.deploy(svccfg)
  */

  def cleanup(log: Logger): Unit = {
    log.info("Cleaning up!")
    ignite.close()
  }

}
