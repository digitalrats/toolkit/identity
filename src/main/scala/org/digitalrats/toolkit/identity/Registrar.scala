package org.digitalrats.toolkit.identity

object Registrar {
  import akka.actor.typed.scaladsl.Behaviors
  import akka.actor.typed.{ActorRef, Behavior, PostStop}
  import akka.http.scaladsl.model.{StatusCode, StatusCodes}
  import com.github.t3hnar.bcrypt._
  import org.slf4j.Logger

  sealed trait Request
  final case class SignRequest(request: SignupRequest, replyTo: ActorRef[StatusCode]) extends Request
  final case object GracefulShutdown extends Request

  final val defaultRole: String = "user"

  private def signup(login: String, password: String, firstName: String, lastName: String, log: Logger): StatusCode = {
    import Model.ignite
      try {
        val salt = generateSalt
        val hash = password.bcrypt(salt).replace(salt,"")
        val model = new Model(salt,hash,Profile(firstName,lastName,defaultRole))
        Model.auth.put(login, model)
        StatusCodes.OK
      } catch {
        case e : Throwable =>
          log.error(e.getLocalizedMessage)
          StatusCodes.BadRequest
      }
  }

  def cleanup(log: Logger): Unit = {
    log.info("Cleaning up!")
  }

  def apply(): Behavior[Request] = Behaviors.receive[Request] { (context, message) => {
    message match {
      case SignRequest(request, replyTo) =>
        replyTo ! signup(request.login, request.password, request.firstName, request.lastName, context.log)
        Behaviors.same
      case GracefulShutdown =>
        context.log.info("Initiating graceful shutdown...")
        Behaviors.stopped { () =>
          cleanup(context.log)
        }
    }
  }}.receiveSignal {
    case (context, PostStop) =>
      context.log.info("Master Control Program stopped")
      cleanup(context.log)
      Behaviors.same
  }

}
