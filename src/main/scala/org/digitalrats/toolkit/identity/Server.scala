package org.digitalrats.toolkit.identity

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorSystem, Behavior, PostStop}
import akka.actor.typed.scaladsl.adapter._
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.ServerBinding
import akka.management.cluster.bootstrap.ClusterBootstrap
import akka.management.scaladsl.AkkaManagement
import akka.stream.ActorMaterializer

import scala.concurrent.{Await, ExecutionContext, ExecutionContextExecutor, Future, Promise}
import scala.io.StdIn
import scala.concurrent.duration._

/*
curl --header "Content-Type: application/json" --request POST --data "{ """login""":"""moyshe""","""password""":"""aeqrfdgh"""}" http://localhost:8080/login
 */

object Server {

  sealed trait Message

  private final case class StartFailed(cause: Throwable) extends Message
  private final case class Started(binding: ServerBinding) extends Message

  case object Stop extends Message

  def apply(host: String, port: Int): Behavior[Message] = Behaviors.setup { ctx =>
    implicit val system: ActorSystem[Nothing] = ctx.system
    implicit val untypedSystem: akka.actor.ActorSystem = ctx.system.toClassic
    implicit val materializer: ActorMaterializer = ActorMaterializer()(untypedSystem)

    val routes = new AuthRoutes(ctx)

    val serverBinding: Future[Http.ServerBinding] =
      Http.apply().bindAndHandle(routes.theAuthRoutes, host, port)
    ctx.pipeToSelf(serverBinding) {
      case scala.util.Success(binding) => Started(binding)
      case scala.util.Failure(ex) => StartFailed(ex)
    }

    def running(binding: ServerBinding): Behavior[Message] =
      Behaviors.receiveMessagePartial[Message] {
        case Stop =>
          ctx.log.info(
            "Stopping server http://{}:{}/",
            binding.localAddress.getHostString,
            binding.localAddress.getPort)
          Behaviors.stopped
      }.receiveSignal {
        case (_, PostStop) =>
          Model.cleanup(system.log)
          binding.unbind()
          Behaviors.same
      }

    def starting(wasStopped: Boolean): Behaviors.Receive[Message] =
      Behaviors.receiveMessage[Message] {
        case StartFailed(cause) =>
          throw new RuntimeException("Server failed to start", cause)
        case Started(binding) =>
          ctx.log.info(
            "Server online at http://{}:{}/",
            binding.localAddress.getHostString,
            binding.localAddress.getPort)
          if (wasStopped) ctx.self ! Stop
          running(binding)
        case Stop =>
          starting(wasStopped = true)
      }

    starting(wasStopped = false)
  }

  def main(args: Array[String]) {
    // Activate Ignite client as soon as possible
    val system: ActorSystem[Server.Message] =
      ActorSystem(Server("localhost", 8080), "IdentityServer")

    implicit val ec: ExecutionContextExecutor = system.executionContext

    AkkaManagement(system).start()
    //ClusterBootstrap(system).start()

    def shutdown(): Unit = {
      system.terminate()
      Await.result(system.whenTerminated, 30 seconds)
    }
    scala.sys.addShutdownHook(() -> shutdown())

    println(s"Press RETURN to stop...")
    StdIn.readLine() // let it run until user presses return
    system.terminate()
  }
}
