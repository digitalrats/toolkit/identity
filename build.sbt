name := "identity"
organization := "org.digitalrats"

version := "0.1"

scalaVersion := "2.12.10"

scalacOptions ++= Seq(
  "-feature",
  "-deprecation",
  "-unchecked",
  "-language:postfixOps",
  "-language:higherKinds",
  "-Ypartial-unification")

javaOptions += "-Xmx512m"

unmanagedJars in Compile += file("../artifacts/AuthService.jar")

val akkaVersion = "2.6.4"
val akkaHttpVersion = "10.1.11"
val akkaManagementVersion = "1.0.6"

libraryDependencies ++= Seq {
  "ch.qos.logback" % "logback-classic" % "1.2.3"
}

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
)

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-slf4j",
  "com.typesafe.akka" %% "akka-actor-typed",
  "com.typesafe.akka" %% "akka-stream",
  "com.typesafe.akka" %% "akka-persistence",
  "com.typesafe.akka" %% "akka-cluster-typed",
  "com.typesafe.akka" %% "akka-cluster-sharding",
  "com.typesafe.akka" %% "akka-protobuf",
  "com.typesafe.akka" %% "akka-coordination",
  "com.typesafe.akka" %% "akka-discovery",
).map(_ % akkaVersion)

libraryDependencies += "com.typesafe.akka" %% "akka-actor-testkit-typed" % akkaVersion % "test"

libraryDependencies ++= Seq(
  "com.lightbend.akka.management" %% "akka-management",
  "com.lightbend.akka.management" %% "akka-management-cluster-http",
  "com.lightbend.akka.management" %% "akka-management-cluster-bootstrap",
  "com.lightbend.akka.discovery" %% "akka-discovery-consul"
).map(_ % akkaManagementVersion)

libraryDependencies ++= Seq(
  "org.scalactic" %% "scalactic" % "3.1.1",
  "org.scalatest" %% "scalatest" % "3.1.1" % "test"
)

val catsVersion = "2.1.1"

libraryDependencies ++= Seq(
  "org.typelevel" %% "cats-core" % catsVersion,
)

val circeVersion = "0.13.0"
libraryDependencies ++= Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser",
).map(_ % circeVersion)

libraryDependencies ++= Seq(
  "com.pauldijou" %% "jwt-circe" % "4.3.0"
)

libraryDependencies ++= Seq(
  "de.heikoseeberger" %% "akka-http-circe" % "1.31.0"
)

libraryDependencies += "com.github.t3hnar" %% "scala-bcrypt" % "4.1"

libraryDependencies += "com.bettercloud" % "vault-java-driver" % "5.1.0"

val igniteVersion = "2.8.0"
libraryDependencies ++= Seq(
  "org.apache.ignite" % "ignite-core",
  "org.apache.ignite" % "ignite-slf4j"
).map(_ % igniteVersion)

Revolver.settings
